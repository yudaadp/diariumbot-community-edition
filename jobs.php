<?php
date_default_timezone_set ( "Asia/Jakarta" );

include_once 'config/app.php';

$time  = date('H:i');
$msg  = 'N/A';

$dgtl = new dgtl_diAriumTelkom();

$dgtl->username = APPUSER;
$dgtl->password = APPPASS;

$postdata = $dgtl->loadDraftPost($time);


if($postdata) {
    foreach ($postdata as $val) {
        //if(!$dgtl->checkLoginStatus(USERNAME)) {
            $dgtl->do_login($dgtl->get_diArium_token(DIARIUM_LOGIN), true);
        //}

        $dgtl->do_post(str_replace('[DAY]', $dgtl->dayID(), $val['post_txt']), DIARIUM_POST);
    }

    $msg = 'Executed';
//var_dump($postdata);
}

echo $msg;
?>