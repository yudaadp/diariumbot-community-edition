/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 10.1.35-MariaDB : Database - diarium_
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `dgtl_activity_logs` */

DROP TABLE IF EXISTS `dgtl_activity_logs`;

CREATE TABLE `dgtl_activity_logs` (
  `id` varchar(35) DEFAULT NULL,
  `userid` varchar(8) DEFAULT NULL,
  `activity_nm` varchar(25) DEFAULT NULL,
  `activity_dtl` text,
  `created_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dgtl_activity_logs` */

/*Table structure for table `dgtl_draftpost` */

DROP TABLE IF EXISTS `dgtl_draftpost`;

CREATE TABLE `dgtl_draftpost` (
  `id` varchar(35) NOT NULL,
  `post_txt` text,
  `sch_post_tm` varchar(5) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `createby` varchar(12) DEFAULT NULL,
  `off_sts` enum('Y','N') DEFAULT 'N',
  `userid` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dgtl_draftpost` */

insert  into `dgtl_draftpost`(`id`,`post_txt`,`sch_post_tm`,`createdate`,`createby`,`off_sts`,`userid`) values ('55513b09-c6f3-11e8-95c7-80a589783a9','Selamat hari [DAY] SigmaExpert, selamat memulai aktivitas hari ini. Salam 3M!!','07:00','2018-10-03 17:01:12','DGTL','N',NULL),('9bcb47c2-c6f3-11e8-95c7-80a589783a9','Break time!! Pergunakan waktu istirahat sengan sebaiknya.','11:50','2018-10-03 17:03:12','DGTL','N',NULL),('b7424239-c6f3-11e8-95c7-80a589783a9','Selamat malam, selamat beristirahat rekan-rekan semua.','21:30','2018-10-03 17:04:00','DGTL','N',NULL),('ec2216d4-c6f6-11e8-95c7-80a589783a9','Semangat!!!!','10:22','2018-10-03 17:26:57','DGTL','Y',NULL);

/*Table structure for table `dgtl_group_words` */

DROP TABLE IF EXISTS `dgtl_group_words`;

CREATE TABLE `dgtl_group_words` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `group_cd` varchar(5) DEFAULT NULL,
  `keyword` varchar(50) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `createby` varchar(20) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `last_updateby` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `dgtl_group_words` */

insert  into `dgtl_group_words`(`id`,`group_cd`,`keyword`,`createdate`,`createby`,`last_update`,`last_updateby`) values (1,'ME','aku','2018-10-11 16:07:56','YUDA',NULL,NULL),(2,'ME','saya','2018-10-11 16:14:14','YUDA',NULL,NULL),(3,'ME','gw','2018-10-11 16:14:14','YUDA',NULL,NULL),(4,'ME','gue','2018-10-11 16:14:14','YUDA',NULL,NULL),(5,'ME','abdi','2018-10-11 16:14:14','YUDA',NULL,NULL),(6,'ME','me','2018-10-11 16:14:14','YUDA',NULL,NULL),(7,'ME','guwe','2018-10-11 16:14:14','YUDA',NULL,NULL),(8,'ME','saya','2018-10-11 16:14:14','YUDA',NULL,NULL),(9,'AYO','yo','2018-10-11 16:18:29','YUDA',NULL,NULL),(10,'AYO','ayo','2018-10-11 16:18:29','YUDA',NULL,NULL),(11,'AYO','kuy','2018-10-11 16:18:29','YUDA',NULL,NULL),(12,'AYO','yuks','2018-10-11 16:18:29','YUDA',NULL,NULL),(13,'AYO','yuks ah','2018-10-11 16:18:29','YUDA',NULL,NULL),(14,'SMGT','semangat','2018-10-11 16:23:42','YUDA',NULL,NULL),(15,'SMGT','semangatt','2018-10-11 16:23:42','YUDA',NULL,NULL),(16,'SMGT','semangatttt','2018-10-11 16:23:42','YUDA',NULL,NULL),(17,'SMGT','semangattttt','2018-10-11 16:23:42','YUDA',NULL,NULL),(18,'SMGT','semangat!!','2018-10-11 16:23:42','YUDA',NULL,NULL),(19,'SMGT','seemaangaat','2018-10-11 16:23:42','YUDA',NULL,NULL),(20,'SMGT','semangaaat?','2018-10-11 16:23:42','YUDA',NULL,NULL);

/*Table structure for table `dgtl_mis` */

DROP TABLE IF EXISTS `dgtl_mis`;

CREATE TABLE `dgtl_mis` (
  `id` char(36) NOT NULL,
  `refid` varchar(10) DEFAULT NULL,
  `mis_desc` varchar(60) DEFAULT NULL,
  `trip_cost` varchar(20) DEFAULT NULL,
  `mis_sts` varchar(30) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ` (`refid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dgtl_mis` */

/*Table structure for table `dgtl_parameter_criteria` */

DROP TABLE IF EXISTS `dgtl_parameter_criteria`;

CREATE TABLE `dgtl_parameter_criteria` (
  `id` varchar(35) NOT NULL,
  `arr_group` text,
  `answr` text,
  `createdate` datetime DEFAULT NULL,
  `createby` varchar(20) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `last_updateby` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dgtl_parameter_criteria` */

insert  into `dgtl_parameter_criteria`(`id`,`arr_group`,`answr`,`createdate`,`createby`,`last_update`,`last_updateby`) values ('31f4ac8f-cd38-11e8-95c7-80a589783a9','\'ME\',\'SMGT\'','Semangat!!!','2018-10-11 16:28:19','YUDA',NULL,NULL);

/*Table structure for table `dgtl_postdata` */

DROP TABLE IF EXISTS `dgtl_postdata`;

CREATE TABLE `dgtl_postdata` (
  `id` varchar(35) NOT NULL,
  `post_id` varchar(10) DEFAULT NULL,
  `post_usr` varchar(50) DEFAULT NULL,
  `post_txt` text,
  `post_res` text,
  `proc_sts` enum('R','P','N') DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dgtl_postdata` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
