<?php
define("HOST", "127.0.0.1");
define("DBUSER", "root");
define("DBPASS", "");
define("DATABASE", "diarium_");

class dgtl_dbconn {

  private $host;
  private $user;
  private $password;
  private $db;
  private $conn;

  public function __construct($params = []) {
    $this->host     = HOST;
    $this->user     = DBUSER;
    $this->password = DBPASS;
    $this->db       = DATABASE;

    $this->conn = $mysqli = new mysqli($this->host, $this->user, $this->password, $this->db, 3306);

    if ($mysqli->connect_error) {
      trigger_error('Error connection: ' . $mysqli->connect_error, E_USER_ERROR);
    }
  }

  public function connect() {
    return $this->conn;
  }
}
?>