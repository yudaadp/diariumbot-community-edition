<?php
//diarium
define('DIARIUM_LOGIN', 'https://diarium.telkom.co.id/login');
define('DIARIUM_POST', 'https://diarium.telkom.co.id/post/add');
define('DIARIUM_HOMEPAGE', 'https://diarium.telkom.co.id/home');
define('DIARIUM_COMMENT_ADD', 'https://diarium.telkom.co.id/comment/add');
define('DIARIUM_LIKE_POST', 'https://diarium.telkom.co.id/like/post/');
define('DIARIUM_ADD_RECOGNITION', 'https://diarium.telkom.co.id/recognition/add');
define('DIARIUM_DELETE_POST', 'https://diarium.telkom.co.id/post/delete/');

//MOBILE
define('M_LOGIN', 'https://apifactory.telkom.co.id:8243/hcm/api/diarium/authorization/v1/oauth/token');
define('M_ACC_INFO', 'https://apifactory.telkom.co.id:8243/hcm/api/diarium/authorization/v1/user');
define('M_CHECKIN', 'https://myworkbook.telkom.co.id/mwb/geni/index.php?r=prsv144/checkin');

//mis
define('MIS_LOGIN', 'https://mis.telkomsigma.co.id/');
define('MIS_CASHOUT', 'https://mis.telkomsigma.co.id/_cashout/cashout_list_act.php?_year=2018&_catipe=&_pos=ALL&_cmpy=&_position=&q=&url=4=84');
?>