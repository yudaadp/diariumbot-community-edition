<?php
/*
 * diArium_bot v1.0 beta - 29-09-2018 yudaadp@gmail.com
 *  - add auto post
 *  - add auto comment timeline
 *  - add auto like post
 */

include_once ('../config/app.php')

$dgtl_diArium   = new dgtl_diAriumTelkom();
$dgtl_htmlArium = new simple_html_dom();

$dgtl_diArium->username = APPUSER;
$dgtl_diArium->password = APPPASS;

$dgtl_res = $dgtl_diArium->do_login($dgtl_diArium->get_diArium_token(DIARIUM_LOGIN), DIARIUM_LOGIN);

$dgtl_diArium->addRecognition('22291');
?>