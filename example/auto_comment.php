<?php
/*
 * diArium_bot v1.0 beta - 29-09-2018 yudaadp@gmail.com
 *  - add auto post
 *  - add auto comment timeline
 *  - add auto like post
 */

require_once ('../config/app.php');

$dgtl_diArium   = new dgtl_diAriumTelkom();
$dgtl_htmlArium = new simple_html_dom();

$dgtl_diArium->username = APPUSER;
$dgtl_diArium->password = APPPASS;

$dgtl_res = $dgtl_diArium->do_login($dgtl_diArium->get_diArium_token(DIARIUM_LOGIN), DIARIUM_LOGIN);

$dgtl_htmlArium->load($dgtl_res);

foreach($dgtl_htmlArium->find("div.post-media") as $row) {
    $arr = explode('-', $row->attr['id']);
    $post_id = $arr[1];

    $dgtl_diArium->auto_comment($post_id, DIARIUM_COMMENT_ADD);
    $dgtl_diArium->auto_like($post_id, DIARIUM_LIKE_POST);
}
?>